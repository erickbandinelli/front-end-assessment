![Alt text](https://s3.amazonaws.com/hww-static/horizontal-logo-transparent-black.png)

Frontend Assessment - Hogarth Worldwide - Brasil
==============================================

This assessment usualy takes about a day to complete, when you receive this you will have 24 hours to send the results, if you don't finish all don't worry please send what you did. Please start forking this public project into your bitbucket account and make the changes in a branch named {YourName_assessment} and after you finished please create a merge request from the forked repo.

We'll evaluate the test consedering the bellow itens:

1. Code Quality & Organization
2. Speed and Overall Performance
3. Image Generation Procedures
4. HTML Techniques

The test is broken in 3 main parts, Layout Mobile, Desktop and HTML/Stylesheet construction.

## Briefing:

As a client we need you to develop two layouts for us, the tecnology that you going to use is described in the Tech Stack session, feel free to add up to that stack, like sass, less, gulp, grunt. You going to receive the layouts files as a PSD (Photoshop) using the Mobile first aproach procude those two layouts and all it's components in a responsive manner. Some itens are required, such as:

* The sliders should be responsive
* Bootstrap and it's components must be used
* Resolution Destkop will be considered: (1024x768) and Mobile (320x568)

Feel free to add Jquery and it's plugins if you feel necessary, as a final add in the end of this readme an description on how to run the project and what tecnology has been used.

### Required Tech Stack:

* HTML 5
* CSS 3
* Standard Javascript
* Bootstrap

### INSTRUCTIONS

* I did not download the dependencies with NPM because my internet was reduced and there are many packages that download and was not downloading.