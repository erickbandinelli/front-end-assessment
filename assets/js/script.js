$(document).ready(function(){
    $('.icon-menu-responsive').click(function(){
        $('.menu-options').addClass('menu-options-active');
        $('.menu-options').addClass('animated flipInY');
        $('body').addClass('active');
        $('.btn-close').addClass('btn-close-active');
    });

    $('.btn-close').click(function(){
        $('.menu-options').removeClass('menu-options-active');
        $('.menu-options').removeClass('animated flipInY');
        $('body').removeClass('active');
        $('.btn-close').removeClass('btn-close-active');
    });

    $('.owl-carousel').owlCarousel({
        loop:false,
        margin:10,
        nav:true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:1
            },
            1000:{
                items:2
            }
        }
    })
});